Name:           autosig-u-boot
Version:        2023.04
Release:        4%{?dist}
Summary:        U-boot with android boot support for qemu

License:        GPL-2.0+

Source0:        https://source.denx.de/u-boot/u-boot/-/archive/v2023.04/u-boot-v2023.04.tar.gz
Patch1:         0001-disk-part_efi-provide-read_gpt_table.patch
Patch2:         0002-disk-part_efi-zero-crc32-before-hashing.patch
Patch3:         0003-cmd-ab-implement-qualcomm-ab-slot-logic.patch
Patch4:         0004-arm-qemu-bootcmd-for-qualcomm-ab-boot.patch
Patch5:         0005-arm-qemu-amend-bootcmd-for-qualcomm-ab-boot.patch

BuildRequires:  gcc make swig bison flex python3-devel openssl-devel python3-setuptools

%define debug_package %{nil}

%ifarch aarch64
%define uboot_arch arm64
%define uboot_config qemu_%{uboot_arch}_defconfig
%else
%define uboot_arch %{_arch}
%define uboot_config qemu-%{uboot_arch}_defconfig
%endif

%description
This is a build of u-boot for the purpose of using with qemu to
demonstrate booting using android boot partitions (aboot).
Do not use in physical systems.

%prep
%autosetup -p 1 -n u-boot-v%{version}

echo 'CONFIG_ANDROID_AB=y' >> configs/%{uboot_config}
echo 'CONFIG_ANDROID_BOOT_IMAGE=y' >> configs/%{uboot_config}
echo 'CONFIG_BOOTCOMMAND="run bootcmd_ab_qcom"' >> configs/%{uboot_config}
echo 'CONFIG_CMD_ABOOTIMG=y' >> configs/%{uboot_config}
echo 'CONFIG_CMD_AB_QCOM=y' >> configs/%{uboot_config}
echo 'CONFIG_CMD_AB_SELECT=y' >> configs/%{uboot_config}
echo 'CONFIG_CMD_ADTIMG=y' >> configs/%{uboot_config}
echo 'CONFIG_CMD_BCB=y' >> configs/%{uboot_config}
echo 'CONFIG_CMD_GPT=y' >> configs/%{uboot_config}
echo 'CONFIG_PARTITION_TYPE_GUID=y' >> configs/%{uboot_config}
echo 'CONFIG_QCOM_AB=y' >> configs/%{uboot_config}

%build
%{__make} %{uboot_config}
%make_build

%install
mkdir -p $RPM_BUILD_ROOT/boot

%ifarch x86_64
# On x86 the .rom file is used
install -DpZm 0644 u-boot.rom $RPM_BUILD_ROOT/boot/u-boot.bin
%else
install -DpZm 0644 u-boot.bin $RPM_BUILD_ROOT/boot/u-boot.bin
%endif

%files
/boot/u-boot.bin

%changelog
* Thu Dec 14 2023 Eric Chanudet <echanude@rehdat.com> - 2023.04-4
- Pass root=PARTLABEL=system_a for all slots to simulate firmware behavior.
* Mon Jun 26 2023 Eric Curtin <ecurtin@redhat.com> - 2023.04-3
- Ensure androidboot.bootdevice= gets added to cmdline by u-boot.
* Wed Jun 21 2023 Eric Chanudet <echanude@redhat.com> - 2023.04-2
- Re-introduce configurations dropped during the last upgrade.
* Tue Jun 13 2023 Eric Chanudet <echanude@redhat.com> - 2023.04-1
- Upgrade to 2023.04 and add Qualcomm A/B switching logic.
* Fri Aug 26 2022 Alexander Larsson <alexl@redhat.com> - 2022.07-1
- Initial version with added aboot patches
